import board
import neopixel
import adafruit_fancyled.adafruit_fancyled as fancy

pixel_pin = board.D18
num_pixels = 300
ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)

# Declare a 4-element color palette, this one happens to be a
# 'blackbody' palette -- good for heat maps and firey effects.
palette = [
    fancy.CRGB(1.0, 1.0, 1.0),  # White
    fancy.CRGB(1.0, 1.0, 0.0),  # Yellow
    fancy.CRGB(1.0, 0.0, 0.0),  # Red
    fancy.CRGB(0.0, 0.0, 0.0),
]  # Black

offset = 0  # Positional offset into color palette to get it to 'spin'
levels = (0.25, 0.3, 0.15)  # Color balance / brightness for gamma function

while True:
    for i in range(num_pixels):
        # Load each pixel's color from the palette using an offset, run it
        # through the gamma function, pack RGB value and assign to pixel.
        color = fancy.palette_lookup(palette, offset + i / num_pixels)
        color = fancy.gamma_adjust(color, brightness=levels)
        pixels[i] = color.pack()
    pixels.show()

    offset += 0.001  # Bigger number = faster spin