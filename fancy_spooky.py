import board
import neopixel
import adafruit_fancyled.adafruit_fancyled as fancy

pixel_pin = board.D18
num_pixels = 300
ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.1, auto_write=False, pixel_order=ORDER
)

# Declare a 6-element RGB rainbow palette
palette = [
    # fancy.CRGB(247, 95, 23),  # Orange 1
    # fancy.CRGB(255, 154, 0),  # Orange 2
    fancy.CRGB(255, 94, 14),  # Orange
    # fancy.CRGB(0, 0, 0),  # Black
    fancy.CRGB(136, 30, 228),  # Purple
    fancy.CRGB(255, 94, 14),  # Orange
    # fancy.CRGB(133, 226, 31),  # Green
]  # Magenta

offset = 0  # Positional offset into color palette to get it to 'spin'

while True:
    for i in range(num_pixels):
        # Load each pixel's color from the palette using an offset, run it
        # through the gamma function, pack RGB value and assign to pixel.
        color = fancy.palette_lookup(palette, offset + i / num_pixels)
        # color = fancy.gamma_adjust(color, brightness=0.25)
        pixels[i] = color.pack()
    pixels.show()

    offset += 0.0025  # Bigger number = faster spin