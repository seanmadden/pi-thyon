import board
import neopixel

pixels = neopixel.NeoPixel(board.D18, 300)
num_pixels = 300
ORDER = neopixel.GRB

for i in range(num_pixels):
    pixels[i] = 0xff0000
