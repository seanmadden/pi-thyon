import time

import boto3
import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
from botocore.config import Config


current_milli_time = lambda: str(int(round(time.time() * 1000)))

# create the spi bus
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)

# create the cs (chip select)
cs = digitalio.DigitalInOut(board.D5)

# create the mcp object
mcp = MCP.MCP3008(spi, cs)

# create an analog input channel on pin 0
chan = AnalogIn(mcp, MCP.P0)

session = boto3.Session()
# Recommended Timestream write client SDK configuration:
#  - Set SDK retry count to 10.
#  - Use SDK DEFAULT_BACKOFF_STRATEGY
#  - Set RequestTimeout to 20 seconds .
#  - Set max connections to 5000 or higher.
write_client = session.client('timestream-write', config=Config(read_timeout=20, max_pool_connections=5000,
                                                                retries={'max_attempts': 10}))


def write_records(raw_light, voltage):
    print("Writing records")
    current_time = current_milli_time()

    dimensions = [
        {'Name': 'sensor', 'Value': 'window-sensor-1'}
    ]

    raw_level = {
        'Dimensions': dimensions,
        'MeasureName': 'raw_light_level',
        'MeasureValue': str(raw_light),
        'Time': current_time
    }

    voltage_level = {
        'Dimensions': dimensions,
        'MeasureName': 'voltage',
        'MeasureValue': str(voltage),
        'Time': current_time
    }

    records = [raw_level, voltage_level]

    try:
        result = write_client.write_records(DatabaseName='natural-window-light', TableName='window-light-sensor',
                                            Records=records, CommonAttributes={})
        print("WriteRecords Status: [%s]" % result['ResponseMetadata']['HTTPStatusCode'])
    except Exception as err:
        print("Error:", err)


while True:
    print('Raw ADC Value: ', chan.value)
    print('ADC Voltage: ' + str(chan.voltage) + 'V')
    write_records(chan.value, chan.voltage)
    time.sleep(0.5)
