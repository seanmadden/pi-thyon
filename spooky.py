# Simple test for NeoPixels on Raspberry Pi
import time
import board
import neopixel
import adafruit_fancyled.adafruit_fancyled as fancy

pixel_pin = board.D18
num_pixels = 300
ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.10, auto_write=False, pixel_order=ORDER
)


orange = fancy.CRGB(255, 79, 0)  # Orange
purple = fancy.CRGB(160, 32, 240)  # Purple

# colors = list(black.range_to(orange, 300))

color = orange
counter = 0
group_size = 2
for i in range(num_pixels):
    counter += 1
    if counter >= group_size:
        counter = 0
        color = purple if color != purple else orange
    pixels[i] = color.pack()
pixels.show()


